package org.mycompany;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Policy {
    
    @NotNull
    @Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}")
    private String created_on;

    @NotNull
    private String effectuates_on;

    @NotNull
    private String name;

    @NotNull
    @Min(1)
    private int payment_rate;

    @NotNull
    @Min(0)
    private double premium;

    @NotNull
    @Size(min=3, max=3)
    private String premium_currency;

    public Policy() {
    }

    public Policy(String created_on, String effectuates_on, String name, int payment_rate, double premium, String premium_currency) {
        this.created_on = created_on;
        this.effectuates_on = effectuates_on;
        this.name = name;
        this.payment_rate = payment_rate;
        this.premium = premium;
        this.premium_currency = premium_currency;
    }

    public String getCreated_on() {
        return this.created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getEffectuates_on() {
        return this.effectuates_on;
    }

    public void setEffectuates_on(String effectuates_on) {
        this.effectuates_on = effectuates_on;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayment_rate() {
        return this.payment_rate;
    }

    public void setPayment_rate(int payment_rate) {
        this.payment_rate = payment_rate;
    }

    public double getPremium() {
        return this.premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }

    public String getPremium_currency() {
        return this.premium_currency;
    }

    public void setPremium_currency(String premium_currency) {
        this.premium_currency = premium_currency;
    }
    /** 
    public Policy created_on(String created_on) {
        this.created_on = created_on;
        return this;
    }

    public Policy effectuates_on(String effectuates_on) {
        this.effectuates_on = effectuates_on;
        return this;
    }

    public Policy name(String name) {
        this.name = name;
        return this;
    }

    public Policy payment_rate(int payment_rate) {
        this.payment_rate = payment_rate;
        return this;
    }

    public Policy premium(double premium) {
        this.premium = premium;
        return this;
    }

    public Policy premium_currency(String premium_currency) {
        this.premium_currency = premium_currency;
        return this;
    }
    */
    
    @Override
    public String toString() {
        return "{" +
            " created_on='" + getCreated_on() + "'" +
            ", effectuates_on='" + getEffectuates_on() + "'" +
            ", name='" + getName() + "'" +
            ", payment_rate='" + getPayment_rate() + "'" +
            ", premium='" + getPremium() + "'" +
            ", premium_currency='" + getPremium_currency() + "'" +
            "}";
    }

}