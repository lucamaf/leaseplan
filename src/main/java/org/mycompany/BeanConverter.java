package org.mycompany;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class BeanConverter implements Processor{

    @Override
    public void process(Exchange ex0) throws Exception {
        Policy_fr pFr = new Policy_fr();
        Policy p = ex0.getIn().getBody(Policy.class);
        pFr.setCreation_date(p.getCreated_on());
        pFr.setName(p.getName());
        pFr.setPayment_per_year(p.getPayment_rate());
        pFr.setPayment_value(p.getPremium());
        int day = Integer.parseInt(p.getEffectuates_on().substring(8, 10));
        int month = Integer.parseInt(p.getEffectuates_on().substring(5, 7));
        int year = Integer.parseInt(p.getEffectuates_on().substring(0, 4));
        pFr.setStarts_on_day(day);
        pFr.setStarts_on_month(month);
        pFr.setStarts_on_year(year);
        ex0.getIn().setBody(pFr);
    }
    
}