package org.mycompany;

public class Policy_fr {
    private String creation_date;
    private String name;
    private int payment_per_year;
    private double payment_value;
    private int starts_on_day;
    private int starts_on_month;
    private int starts_on_year;
    private int insurance_id;

    public Policy_fr() {
    }

    public Policy_fr(String creation_date, String name, int payment_per_year, double payment_value, int starts_on_day, int starts_on_month, int starts_on_year, int insurance_id) {
        this.creation_date = creation_date;
        this.name = name;
        this.payment_per_year = payment_per_year;
        this.payment_value = payment_value;
        this.starts_on_day = starts_on_day;
        this.starts_on_month = starts_on_month;
        this.starts_on_year = starts_on_year;
        this.insurance_id = insurance_id;
    }

    public String getCreation_date() {
        return this.creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayment_per_year() {
        return this.payment_per_year;
    }

    public void setPayment_per_year(int payment_per_year) {
        this.payment_per_year = payment_per_year;
    }

    public double getPayment_value() {
        return this.payment_value;
    }

    public void setPayment_value(double payment_value) {
        this.payment_value = payment_value;
    }

    public int getStarts_on_day() {
        return this.starts_on_day;
    }

    public void setStarts_on_day(int starts_on_day) {
        this.starts_on_day = starts_on_day;
    }

    public int getStarts_on_month() {
        return this.starts_on_month;
    }

    public void setStarts_on_month(int starts_on_month) {
        this.starts_on_month = starts_on_month;
    }

    public int getStarts_on_year() {
        return this.starts_on_year;
    }

    public void setStarts_on_year(int starts_on_year) {
        this.starts_on_year = starts_on_year;
    }

    public int getInsurance_id() {
        return this.insurance_id;
    }

    public void setInsurance_id(int insurance_id) {
        this.insurance_id = insurance_id;
    }
    /** 
    public Policy_fr creation_date(String creation_date) {
        this.creation_date = creation_date;
        return this;
    }

    public Policy_fr name(String name) {
        this.name = name;
        return this;
    }

    public Policy_fr payment_per_year(int payment_per_year) {
        this.payment_per_year = payment_per_year;
        return this;
    }

    public Policy_fr payment_value(double payment_value) {
        this.payment_value = payment_value;
        return this;
    }

    public Policy_fr starts_on_day(int starts_on_day) {
        this.starts_on_day = starts_on_day;
        return this;
    }

    public Policy_fr starts_on_month(int starts_on_month) {
        this.starts_on_month = starts_on_month;
        return this;
    }

    public Policy_fr starts_on_year(int starts_on_year) {
        this.starts_on_year = starts_on_year;
        return this;
    }

    public Policy_fr insurance_id(int insurance_id) {
        this.insurance_id = insurance_id;
        return this;
    }

    */

    @Override
    public String toString() {
        return "{" +
            " creation_date='" + getCreation_date() + "'" +
            ", name='" + getName() + "'" +
            ", payment_per_year='" + getPayment_per_year() + "'" +
            ", payment_value='" + getPayment_value() + "'" +
            ", starts_on_day='" + getStarts_on_day() + "'" +
            ", starts_on_month='" + getStarts_on_month() + "'" +
            ", starts_on_year='" + getStarts_on_year() + "'" +
            ", insurance_id='" + getInsurance_id() + "'" +
            "}";
    }

}