
# Spring-Boot Camel QuickStart

This example demonstrates how you can use Apache Camel with Spring Boot.

The quickstart uses Spring Boot to configure a little application that includes a Camel route that exposes 2 REST APIs and allows for several read operations on a mysql-like database and enfornces tx when performing a write operation, going through a messaging infrastructure.

### Assumptions for running locally

- activemq running locally
- mariadb running locally with predefined database
- created the stored procedures beforehand on the database (added string to date conversion in the stored procedure)

you can start activemq service and console by running:

    ./bin/activemq start
    ./bin/activemq console (login admin:admin)

### Running standalone
The example can be built and run with

    mvn spring-boot:run

### Building

The example can be built with

    mvn clean install

### Running the example in OpenShift

It is assumed that:
- OpenShift platform is already running, if not you can find details how to [Install OpenShift at your site](https://docs.openshift.com/container-platform/3.3/install_config/index.html).
- Your system is configured for Fabric8 Maven Workflow, if not you can find a [Get Started Guide](https://access.redhat.com/documentation/en/red-hat-jboss-middleware-for-openshift/3/single/red-hat-jboss-fuse-integration-services-20-for-openshift/)

The example can be built and run on OpenShift using a single goal:

    mvn fabric8:deploy

When the example runs in OpenShift, you can use the OpenShift client tool to inspect the status

To list all the running pods:

    oc get pods

Then find the name of the pod that runs this quickstart, and output the logs from the running pods with:

    oc logs <name of pod>

You can also use the OpenShift [web console](https://docs.openshift.com/container-platform/3.3/getting_started/developers_console.html#developers-console-video) to manage the
running pods, and view logs and much more.

### Running via an S2I Application Template

Application templates allow you deploy applications to OpenShift by filling out a form in the OpenShift console that allows you to adjust deployment parameters.  This template uses an S2I source build so that it handle building and deploying the application for you.

First, import the Fuse image streams:

    oc create -f https://raw.githubusercontent.com/jboss-fuse/application-templates/GA/fis-image-streams.json

Then create the quickstart template:

    oc create -f https://raw.githubusercontent.com/jboss-fuse/application-templates/GA/quickstarts/spring-boot-camel-template.json

Now when you use "Add to Project" button in the OpenShift console, you should see a template for this quickstart. 

## Lessons learnt

- be careful to use version springboot-1 of hawtio otherwise there is a bug when loading the core hawtio app
- don't import Tomcat but instead use Undertow as web server (see dependency in pom.xml)
- be careful when using swagger to always import also the ui webapp if you want to render the swagger-ui
- changed the default spring-boot:run goal to make it point to dev properties by default
- careful with spring-boot version (it is not 2.0 for this project)
- had to perform a mysql_upgrade on the database because it was running into issue when creating the stored procedure


## Reference material & concepts

- mariadb connection: https://mariadb.com/kb/en/java-connector-using-maven/
- REST component: https://tomd.xyz/camel-rest/<br/>https://camel.apache.org/manual/latest/servlet-tomcat-example.html
- activemq connection:  http://nirmalbalasooriya.blogspot.com/2016/12/red-hat-jboss-fuse-activemq-simple.html<br/>
https://activemq.apache.org/sample-camel-routes
- REST 2 DB:  https://github.com/Talend/apache-camel/tree/master/examples/camel-example-restlet-jdbc
- swagger component: https://github.com/camelinaction/camelinaction2/blob/master/chapter10/servlet-swagger-xml/src/main/resources/camel.xml
- Bean validation: https://blog.sodifrance.fr/bean-validation-avec-camel-composant-camel-bean-validator<br/> https://tomd.xyz/camel-bean-validation/
- REST validation errors: https://stackoverflow.com/questions/49010933/apache-camel-rest-dsl-validating-request-payload-and-return-error-response
- JTA transaction manager: https://www.atomikos.com/Documentation/SpringIntegration<br/>
https://github.com/nielspeter/atomikos-jta-camel-jpa-jms-example/blob/master/src/main/resources/applicationContext.xml